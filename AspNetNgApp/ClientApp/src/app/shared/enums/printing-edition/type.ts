export enum PrintingEditionType {
    Arts = 1,
    Biographies = 2,
    Children = 3,
    Cookbook = 4,
    History = 5,
    Literature = 6,
    Romance = 7,
    SciFi = 8
}
