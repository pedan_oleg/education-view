import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
    PrintingEditionListComponent,
    DetailsComponent
} from '.';

const printingEditionRoutes: Routes = [
    { path: '', component: PrintingEditionListComponent, data: { animation: 'Catalog' } },
    { path: ':query', component: PrintingEditionListComponent, data: { animation: 'Catalog' } },
    { path: 'Details/:id', component: DetailsComponent, data: { animation: 'PrintingEditionDetails' } },
];


@NgModule({
    imports: [
        RouterModule.forChild(printingEditionRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class PrintingEditionRoutingModule { }
