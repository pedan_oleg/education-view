import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageRoutingModule } from './home-page-routing.module';
import { MaterialElementsModule } from 'src/app/shared/modules';

import { HomePageComponent } from './home-page/home-page.component';
import { BestsellerCardComponent } from './bestseller-card/bestseller-card.component';
import { PrintingEditionService } from 'src/app/shared/services';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [HomePageComponent, BestsellerCardComponent, FooterComponent],
  imports: [
    CommonModule,
    MaterialElementsModule,
    HomePageRoutingModule
  ],
  providers: [PrintingEditionService]
})
export class HomePageModule { }
