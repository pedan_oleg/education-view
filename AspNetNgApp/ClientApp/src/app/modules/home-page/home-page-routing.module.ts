import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from 'src/app/modules/home-page/home-page/home-page.component';

const homeRoutes: Routes = [
    { path: '', component: HomePageComponent, data: { animation: 'Home' } }
];

@NgModule({
    imports: [
        RouterModule.forChild(homeRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class HomePageRoutingModule { }