import { Component, Input } from '@angular/core';
import { PrintingEditionModelItem } from 'src/app/shared/models';

@Component({
    selector: 'app-bestseller-card',
    templateUrl: './bestseller-card.component.html',
    styleUrls: ['./bestseller-card.component.scss']
})
export class BestsellerCardComponent {
    @Input() printingEdition: PrintingEditionModelItem;

    public constructor() {

    }
}