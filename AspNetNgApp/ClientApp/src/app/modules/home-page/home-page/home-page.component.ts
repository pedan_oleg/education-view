import { Component, OnInit } from '@angular/core';
import { PrintingEditionModel } from 'src/app/shared/models';
import { PrintingEditionService } from 'src/app/shared/services';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
    protected dataModel: PrintingEditionModel;
    protected dataModel2: PrintingEditionModel;
    protected isVideoPlaying: boolean;

    constructor(private dataService: PrintingEditionService) {
        this.isVideoPlaying = false;
    }

    ngOnInit() {
        this.dataService.getFive(0).subscribe((data: PrintingEditionModel) => {
            this.dataModel = data;
        });

        this.dataService.getFive(4).subscribe((data: PrintingEditionModel) => {
            this.dataModel2 = data;
        });
    }

    toggleVideo() {
        this.isVideoPlaying = !this.isVideoPlaying;
        if (this.isVideoPlaying) {
            // @ts-ignore
            document.getElementById('static_video').play();
            return;
        }
        // @ts-ignore
        document.getElementById('static_video').pause();
    }
}
